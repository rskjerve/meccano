const fs= require('fs');
const webtoken = require('jsonwebtoken')
const private  = fs.readFileSync('./resources/private.key', 'utf8');
const public  = fs.readFileSync('./resources/public.key', 'utf8');
const log = require('color-log')
const log_prefix ="JWT middleware: "
const _ = require('lodash');
let options = { issuer:'meccano_backend',
                expiresIn:  "1h",
                algorithm:  "RS256", };

jwt = {}

jwt.register =(auth_result)=>{

     let payload ={
         sub : auth_result.card,
         name: auth_result.customer,
         balance:auth_result.balance
     }
    return webtoken.sign(payload,private,options)
}

jwt.auth = (req,res,next) => {

     if(!req.headers.authorization)
     {
          log.error('request without authorization header');
          res.status(400);
          res.json({message:'bad request: authorization header missing'})
          return;
      }

    var authHeader = _.trim(req.headers.authorization,"'");
    authHeader =authHeader.split(" ")[1];
    jwt.verifyToken(authHeader)
        .then((result)=>{
            log.info('Token authorized');
            req.card =result.sub;
            next();
        })
        .catch(err =>{

            if(err.name === 'JsonWebTokenError'){
                log.error('invalid token');
                res.status(401);
                res.json({message:'invalid token'})
            }
            else if(err.name === 'TokenExpiredError'){
                log.error('token expired');
                res.status(403);
                res.json({message: 'token expired'})
            }
            else{
                log.error('bad request');
                res.status(400);
                res.json({message: 'other auth problem'})
            }
            return;
    });
}


jwt.verifyToken = token =>
{
    return new Promise((resolve,reject) => {

        webtoken.verify(token, public, options, (err,decoded)=> {
            if(err){
                reject(err)
            }else {
                resolve(decoded);
            }
        });
    });
}

module.exports = jwt;