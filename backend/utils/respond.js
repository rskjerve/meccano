const lodash = require('lodash')
const respond = {};

respond.message = (res,status,message)=>{
    res.status(status);
    res.json({message: message});
    res.send();
};

respond.object = (res,responseObject)=>{
    res.status(responseObject.status);
    delete responseObject.status;
    res.json(responseObject);
    res.send();
}
module.exports = respond;


