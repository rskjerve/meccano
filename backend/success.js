const Success = {
    visa_ok: {status:200, message: 'Visa gift card ordered',id:undefined,balance:undefined},
    order_update_ok: {status: 200},
    setId:(success,id)=>{
        let withId = success;
        withId.id = id;
        return withId;
    },
    setBalanceAndId:(success,id,balance)=>{
        let withBalance = success;
        withBalance.status = 200;
        withBalance.id = id;
        withBalance.balance=balance;
        return withBalance;
    }
}

module.exports =Success;