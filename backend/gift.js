const express = require('express');
const router = express.Router();
const jwt =require('./utils/jwt').auth;
const OrderCalculator = require('./db/OrderCalculator');
const respond = require('./utils/respond');
const log = require('color-log');

router.all('*',jwt);

router.post('/', function(req, res){
    log.info('Received gift post request');

    if(!req.body.amount)
    {
        respond.message(res,400,'request body missing')

    }else {

        OrderCalculator.visa(req.card,req.body.amount)
            .then((result)=>{
               delete result.id;
               log.info('Sending order success response')
               respond.object(res,result)
            })
            .catch(error=>{
                log.info('Sending order error response')
                delete error.id;
                delete error.reason;
                respond.object(res,error)
            });
    }
});


module.exports = router;