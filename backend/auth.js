const express = require('express');
const router = express.Router();
const CardCalculator = require('./db/CardCalculator')
const log = require('fancy-log')
const log_prefix="API auth request: "
const jwt = require('./utils/jwt')



router.post('/', function(req, res){
   CardCalculator.authentication(req.body.card,req.body.pin,result=>{
        if(result.status === 200){
            const jwtToken = jwt.register(result);
            res.json({token:jwtToken});
            res.status(200)
            res.send();
        }
        else if (result.status === 400){
            res.status(401)
            delete result.status;
            res.json(result)
        }
   });
});



//export this router to use in our index.js
module.exports = router;