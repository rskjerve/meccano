const express = require('express');
const router = express.Router();
const jwt =require('./utils/jwt').auth;
const AccountCalculator = require('./db/AccountCalculator');
const Card = require('./db/models/card');
const respond = require('./utils/respond');


router.all('*',jwt);

router.post('/', function(req, res){

    if(!req.body.amount)
    {
        respond.message(res,400,'request body missing')

    }else {
        const card = req.card;
        Card.getAccount(card)
            .then(result => {
              AccountCalculator.withdraw(result.account,req.body.amount)
                  .then( withdrawResult => respond.object(res,withdrawResult))
                  .catch(err => respond.object(res,err))

            })
            .catch(error => {
                respond.message(res,500,'server error');
        });

    }
});


module.exports = router;