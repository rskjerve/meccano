const Errors={
    withdraw_insufficent:{status:400, message:'Insufficent funds, transaction was canceled', reason:'insufficent funds',id:undefined},
    withdraw_unspecified: {status:500, reason:'unspecified withdraw error',id:undefined},
    card_server_error: {status: 500, reason: 'unknown card',id:undefined},
    visa_server_error: {status:500, reason:'could not place order',id:undefined},
    setId:(error,id)=>{
        let withId = error;
        withId.id = id;
        withId.status = 500;
        return withId;
    }
}

module.exports = Errors;