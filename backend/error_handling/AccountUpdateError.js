class AccountUpdateError extends Error {
    constructor(message, status) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.message = message ||
            'An error occured when trying to update Account';

        this.status = status || 500;
    }
}

module.exports=AccountUpdateError;

