// ApplicationError.js
class ImmutableError extends Error {
    constructor(message, status) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.message = message ||
            'Attempting to write to immutable Event doc';

        this.status = status || 500;
    }
}

module.exports=ImmutableError

