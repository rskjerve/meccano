// ApplicationError.js
class DbError extends Error {
    constructor(message, status) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.message = 'Database error:' + message;

        this.status = status || 500;
    }
}

