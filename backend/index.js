const express = require('Express');
const bodyparser = require('body-parser')
const app = express();
const cors = require('cors')
const auth = require('./auth.js');
const withdraw = require('./withdraw');
const gift = require('./gift');
const log = require('fancy-log')
const MongooseSetup = require('./db/setup/MongooseSetup')

MongooseSetup.start();

app.use(bodyparser.json());
//TODO: restrict cors
app.use(cors());
app.use('/auth', auth);
app.use('/withdraw',withdraw);
app.use('/gift',gift);
app.listen(3001)
log('server listening on port 3001');