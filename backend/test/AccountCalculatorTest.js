var expect = require('chai').expect
mongoose = require('mongoose');
const AccountEvent = require('../db/events/account_event');
const Account = require('../db/models/account')
const AccountCalculator = require('../db/AccountCalculator');

describe('AccountCalculator withdraw', (done) =>{
    const number = "57140189244892014"
    const start_amount = 1876
    let account_id;

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                done();
            })
        });
    });

    it('should return correct amount after withdraw', (done)=> {
            const withdraw_amount=700;
            const expectedBalance=start_amount-withdraw_amount;
            AccountCalculator.withdraw(account_id,withdraw_amount)
                .then((result)=>{
                    expect(result.status).to.equal(200);
                    expect(result.balance).to.equal(expectedBalance);
                    done();
                });

    });

    it('should throw error if insufficent funds', done => {
        const withdraw_amount=5678;
        const expectedBalance=start_amount-withdraw_amount;
        AccountCalculator.withdraw(account_id,withdraw_amount)
            .then((result)=>{
                done();
            })
            .catch(error=>{
                expect(error).to.not.be.null;
                done();
            });
    });



    after(done=>{
        mongoose.connection.close();
        done();
    })
});

describe('AccountCalculator deposit', (done) =>{
    const number = "57140189244892014"
    const start_amount = 1876
    let account_id;

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                done();
            })
        });
    });

    it('should return correct amount after deposit', (done)=> {
        const deposit_amount=700;
        const expectedBalance=start_amount+deposit_amount;
        AccountCalculator.deposit(account_id,deposit_amount)
            .then((result)=>{
                expect(result.status).to.equal(200);
                expect(result.balance).to.equal(expectedBalance);
                done();
            });

    });

    it('should throw error if attempting to deposit negative amount', done => {
        const deposit_amount=(-5678);
        AccountCalculator.deposit(account_id,deposit_amount)
            .then((result)=>{
                done();
            })
            .catch(error=>{
                expect(error).to.not.be.null;
                expect(error.status).to.equal(400);
                done()
            });
    });



    after(done=>{
        mongoose.connection.close();
        done();
    })
});

describe('AccountCalculator cancel', (done) =>{
    const number = "57140189244892014"
    const start_amount = 1876
    let account_id;

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                done();
            })
        });
    });

    it('should cancel after insufficent funds on withdraw attempt', done => {
        const withdraw_amount = 5678;
        AccountCalculator.withdraw(account_id,withdraw_amount)
            .then((result)=>{
                expect(result.status).to.not.equal(200);
                done();
            })
            .catch(error=>{
                expect(error).to.not.be.null;
                expect(error.status).to.equal(400);
                Account.findOne({_id:account_id},(err,result)=>{
                    expect(result.balance).to.equal(start_amount);
                    done();
                })
            });
    });



    after(done=>{
        mongoose.connection.close();
        done();
    })
});

describe('AccountEvent reactors', (done) =>{
    const number = "57140189244892014"
    let start_amount = 1876
    let account_id;

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                Account.findOneAndUpdate({_id:result.id},{balance:start_amount},(err,res)=>{
                   start_amount = res.balance;
                   done();
                });
            })
        });
    });

    it('should update account balance after withdraw', (done)=> {
        const withdraw_amount=700;
        const expectedBalance=start_amount-withdraw_amount;
        AccountCalculator.withdraw(account_id,withdraw_amount)
            .then((res)=>{
                Account.findOne({_id:account_id}, (err,result)=>{
                    expect(result.balance).to.equal(expectedBalance);
                    start_amount = start_amount-withdraw_amount;
                    done();
                })
            });
    });

    it('should update Account balance after deposit', (done)=> {
        const deposit_amount=700;
        const expectedBalance=start_amount+deposit_amount;
        AccountCalculator.deposit(account_id,deposit_amount)
            .then((res)=>{
                Account.findOne({_id:account_id}, (err,result)=>{
                    expect(result.balance).to.equal(expectedBalance);
                    done();
                })
            });

    });

    after(done=>{
        mongoose.connection.close();
        done();
    })
});