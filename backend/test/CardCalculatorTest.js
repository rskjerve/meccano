var expect = require('chai').expect
const mongoose = require('mongoose');
const Card = require('../db/models/card');
const Account = require('../db/models/account')
const Customer = require('../db/models/customer')
const dummy = require('mongoose-dummy');
const CardCalculator = require('../db/CardCalculator')
const CardEvent = require ('../db/events/card_event')

describe('Auth attempt',function () {
    const card = dummy(Card, {ignore:['_id','created_at']});
    const customer = dummy(Customer, {ignore:['_id','created_at']});
    const account = dummy(Account, {ignore:['_id','created_at']});

        beforeEach(function (done) {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            Account.create({account_nmbr: account.account_nmbr, balance: account.balance}, (err, result) => {
                Customer.create({first_name: customer.first_name,last_name:customer.last_name, account: result._id}, (err, result) => {
                    Card.create({
                        card_number: card.card_number,
                        pin: card.pin,
                        account: result.account,
                        customer: result._id
                    }, (err, result) => {
                        card._id = result._id;
                        CardEvent.create({card:result._id,event_type:'activated',time:Date.now()}, (err,result)=>{
                            done()
                        });

                    });
                });
            });
        });
    })

        it('should accept auth attempt on correct credentials', function (done) {
            CardCalculator.authentication(card.card_number, card.pin,response => {
                expect(response.status).to.equal(200)
                done();
            }, true);
        });

        it('should reject auth attemp on incorrect credentials', function (done) {
            CardCalculator.authentication(card.card_number, 3425, response => {
                expect(response.status).to.equal(400)
                expect(response.action).to.equal('re-enter')
                done();
            }, true);
        });

        it('should revoke card on 3rd failed auth attempt', function (done) {
            CardCalculator.authentication(card.card_number, 3425, response => {
                    expect(response.status).to.equal(400)
                    expect(response.action).to.equal('re-enter')
                CardCalculator.authentication(card.card_number, 3425, response => {
                    expect(response.status).to.equal(400)
                    expect(response.action).to.equal('re-enter')
                    CardCalculator.authentication(card.card_number, 3985, response => {
                        expect(response.status).to.equal(400)
                        expect(response.action).to.equal('revoke')
                        done();
                    }, true);
                }, true);
            }, true);
            return done();
        });

        it('should revoke card on existing replaced event', function (done) {
           CardEvent.create({card:card._id, event_type:'replaced',time: Date.now()}, (err,result)=>{
               CardCalculator.authentication(card.card_number, card.pin, response => {
                   expect(response.status).to.equal(400)
                   expect(response.action).to.equal('revoke')
                   done();
               }, true);
           })
        });

        it('should revoke card on existing revoked event', function (done) {
        CardEvent.create({card:card._id, event_type:'revoked',time: Date.now()}, (err,result)=>{
            CardCalculator.authentication(card.card_number, card.pin, response => {
                expect(response.status).to.equal(400)
                expect(response.action).to.equal('revoke')
                done();
            }, true);
        })
    });

    afterEach(function (done) {
        mongoose.connection.close();
        done();
    });

});

