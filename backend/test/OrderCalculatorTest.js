var expect = require('chai').expect
mongoose = require('mongoose');
const dummy= require('mongoose-dummy')
const Card = require('../db/models/card')
const Account = require('../db/models/account')
const Customer = require('../db/models/customer')
const Order = require('../db/models/order')
const OrderEvent = require('../db/events/order_event')
const OrderEnum =require('../db/enums').orderModel;


const Errors = require('../error')
const Success = require('../success')
const OrderCalculator = require('../db/OrderCalculator');


describe('OrderCalculator visa', function(done){
    this.timeout(5000);
    const account_number = "57140189244892014"
    const customerDummy= dummy(Customer,{});
    const cardDummy = dummy(Card, {})
    let customer_id;
    let card_id;
    let account_id;
    var order_id;
    var amount;

    let start_amount = 3500

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:account_number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                Customer.create({first_name:customerDummy.first_name, last_name:customerDummy.last_name,
                    account:account_id},(err,customerResult)=>{
                    customer_id=customerResult._id;
                    Card.create({card_number:cardDummy.card_number,pin:1234,account:account_id,customer:customerResult._id},
                        (err,cardResult)=>{
                            if(err){console.log(err)}
                            card_id=cardResult._id;
                            done();
                        });
                })
            })
        });
    });

    it('should return success for a visa order', done => {
        OrderCalculator.visa(cardDummy.card_number,400,false)
            .then(result=>{
                expect(result).to.equal(Success.visa_ok)
                done();
            });
    });

    it('should create db order', done => {
        OrderCalculator.visa(cardDummy.card_number,433,false)
            .then(result=>{
                Order.findOne({'_id':result.id,'status':OrderEnum.status.pending},(err,result)=>{
                    start_amount=start_amount-433;
                    expect(err).to.be.null;
                    expect(result).to.exist;
                    done();
                });
            });
    });

    it('should create db received event', done => {
        OrderCalculator.visa(cardDummy.card_number,489,false)
            .then(result=>{
                start_amount=start_amount-489;
                Order.findOne({'amount':489,'status':OrderEnum.status.pending},(err,result)=>{
                    OrderEvent.findOne({order:result._id,event_type:OrderEnum.event.received},(err,result)=>{
                        expect(err).to.be.null
                        expect(result).to.exist;
                        done();
                    });
                });
            });
    });

    it('should update balance if visa order', done=>{
        const expectBalance= start_amount-440;

        OrderCalculator.visa(cardDummy.card_number,440,false)
            .then(result=>{
                Account.findOne({_id:account_id},(err,result)=>{
                    expect(result.balance).to.equal(expectBalance);
                    start_amount=result.balance;
                    done();
                })
            });
    })

    it('should cancel visa order if insufficent funds',done => {
        OrderCalculator.visa(cardDummy.card_number,4000,false)
            .catch(error=>{
             expect(error.status).to.equal(Errors.withdraw_insufficent.status);
             expect(error.reason).to.equal(Errors.withdraw_insufficent.reason);
             expect(error.message).to.equal(Errors.withdraw_insufficent.message);

             done();
            });
    });

    it('should create cancel event on insufficent funds',done => {
        OrderCalculator.visa(cardDummy.card_number,4000,false)
            .catch(error=>{
                const order_id=error.id;
                OrderEvent.findOne({order:order_id,event_type:OrderEnum.event.rejected},(err,result)=>{
                    expect(err).to.be.null;
                    expect(result).to.exist;
                    done();
                })
            });
    });

    it('should update status to canceled if visa order is rejected', done => {
        OrderCalculator.visa(cardDummy.card_number,4000,false)
            .catch(error=>{
                Order.findOne({_id:error.id,status:OrderEnum.status.canceled},
                    (err,result)=>{
                        expect(result).to.exist;
                        done();
                    });

            });
    });

    it('should update status to delivered if visa order is completed', done => {
        OrderCalculator.visa(cardDummy.card_number,400,false)
            .then(result=>{
                Order.findOne({_id:result.id,status:OrderEnum.status.delivered},
                    (err,result)=>{
                        expect(result).to.exist;
                        done();
                    });
            });
    });



    after(done=>{
        const connection = mongoose.connection;
        connection.db.dropDatabase();
        done();
    })
});