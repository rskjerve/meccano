var expect = require('chai').expect
mongoose = require('mongoose');
const dummy= require('mongoose-dummy')
const Card = require('../db/models/card')
const Account = require('../db/models/account')
const Customer = require('../db/models/customer')

const Order = require('../db/models/order')
const OrderEvent = require('../db/events/order_event')
const OrderEnum =require('../db/enums').orderModel;


describe('Creating new Order', function(done){
    this.timeout(5000);
    const account_number = "57140189244892014"
    const customerDummy= dummy(Customer,{});
    const cardDummy = dummy(Card, {})
    let customer_id;
    let card_id;
    let account_id;

    const start_amount = 18976
        before((done) => {
            mongoose.connect('mongodb://localhost:27017/meccano_test');
            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open',()=>{
                Account.create({account_nmbr:account_number,balance:start_amount},(err,result)=>{
                    account_id = result._id;
                    Customer.create({first_name:customerDummy.first_name, last_name:customerDummy.last_name,
                                 account:account_id},(err,customerResult)=>{
                        customer_id=customerResult._id;
                        Card.create({card_number:cardDummy.card_number,pin:1234,account:account_id,customer:customerResult._id},
                            (err,cardResult)=>{
                                if(err){console.log(err)}
                                card_id=cardResult._id;
                                done();
                        });
                    })
                })
            });
        });

    it('should create a new Order and corresponding OrderEvent', function (done) {
        Order.create({card:card_id,amount:340},(err,result)=>{
            expect(err).to.be.null;
            expect(result.status).to.equal(OrderEnum.status.pending);
            OrderEvent.findOne({order:result._id},(err,result)=>{
               expect(result).to.not.be.null;
               expect(result.event_type).to.equal(OrderEnum.event.received);
               done();
            });
        });
    });

    it('should throw an error if required fields are missing',(done)=>{
         Order.create({amount:600},function(err,result){
             expect(err).to.exist;
             done();
        })
    });

    it('should set order_type to visa if amount is provided',(done)=>{
        Order.create({card:card_id,amount:600},function(err,result){
            expect(result.order_type).to.exist;
            expect(result.order_type).to.equal(OrderEnum.type.visa)
            done();
        })
    });

    it('should set order_type to new_card if amount is not provided',(done)=>{
        Order.create({card:card_id},function(err,result){
            expect(result.order_type).to.exist;
            expect(result.order_type).to.equal(OrderEnum.type.new_card)
            done();
        })
    });

    after(done=>{
        mongoose.connection.close();
        done();
    })
});

describe('OrderEvent Validation', function(done){
    this.timeout(5000);
    const account_number = "57140189244892014"
    const customerDummy= dummy(Customer,{});
    const cardDummy = dummy(Card, {})
    let customer_id;
    let card_id;
    let account_id;
    var order_id;
    var amount;

    const start_amount = 18976
    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:account_number,balance:start_amount},(err,result)=>{
                account_id = result._id;
                Customer.create({first_name:customerDummy.first_name, last_name:customerDummy.last_name,
                    account:account_id},(err,customerResult)=>{
                    customer_id=customerResult._id;
                    Card.create({card_number:cardDummy.card_number,pin:1234,account:account_id,customer:customerResult._id},
                        (err,cardResult)=>{
                            if(err){console.log(err)}
                            card_id=cardResult._id;
                            done();
                        });
                })
            })
        });
    });

    beforeEach(done=>{
        amount = 500
        Order.create({card:card_id,amount:400},(err,result)=>{
            if(err){
                throw err;
            }
            order_id = result._id;
            done();
        })
    })

    it('should reject save() on existing order events',(done)=>{
        OrderEvent.findOne({order:order_id},(err,result)=>{
            result.event_type = OrderEnum.event.canceled;
            result.save((err,result)=>{
               expect(err).to.exist;
               done();
            });
        });
    });

    it('should reject update() on existing order events',(done)=>{
        OrderEvent.update({order:order_id}, { $set: { event_type: OrderEnum.event.sent }}, null,(err,result)=>{
            expect(err).to.exist;
            done();
        })

    });

    it('should reject remove() an order event',(done)=>{
        OrderEvent.findOne({order:order_id},(err,result)=>{
            result.remove((err,result)=>{
                expect(err).to.exist;
                done();
            })
        });
    });


    after(done=>{
        mongoose.connection.close();
        done();
    })
});