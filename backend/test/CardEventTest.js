var expect = require('chai').expect
mongoose = require('mongoose');
const CardEvent = require('../db/events/card_event');
const dummy = require('mongoose-dummy');



describe('CardEvent validation', function(){

    it('should throw error on no card id',(done)=>{
        const nocard = dummy(CardEvent, {});
        const empty_event= new CardEvent({event_type:nocard.event_type,description:nocard.description});

        empty_event.validate(function(err) {
                expect(err.errors.card).to.exist;
                done();
            });
        });

    it('should throw error on no event_type',(done)=>{
        const nocard = dummy(CardEvent, {});
        const empty_event= new CardEvent({card:nocard.card,description:nocard.description});
        empty_event.validate(function(err) {
            expect(err.errors.event_type).to.exist;
            done();
        });
    });
    it('should throw error on incorrect event_type',(done)=>{
        const nocard = dummy(CardEvent, {});
        const empty_event= new CardEvent({card:nocard.card,type:'undefined',description:nocard.description});
        empty_event.validate(function(err) {
            expect(err.errors.event_type).to.exist;
            done();
        });
    });
});

describe('CardEvent db operations', function() {
    const present_event = dummy(CardEvent, {});

    before(function (done) {
        this.enableTimeouts(false)
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            const nocard = dummy(CardEvent, {});
            CardEvent.create({card:present_event.card,description:present_event.description,event_type: present_event.event_type},
                (err,result) =>{
                    return done();
                });
        });
    });

    it('Should create CardEvent doc ', function(done) {
        const nocard = dummy(CardEvent, {});
        CardEvent.create({card:nocard.card,description:nocard.description,event_type: nocard.event_type},
            (err,result) =>{
                expect(err).to.be.null;
                return done();
            });
    });

    it('Should reject save attempts on existing objects', function(done) {
        CardEvent.findOne({},(err,res)=>{
            expect(err).to.be.null;
            expect(res).to.exist
                res.event_type = "auth_success";
                res.save((err)=>{
                    expect(err).to.not.be.null;
                    return done();
                });
        });
    });
});

