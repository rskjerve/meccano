var expect = require('chai').expect
mongoose = require('mongoose');
const AccountEvent = require('../db/events/account_event');
const Account = require('../db/models/account')

describe('Creating new account', (done) =>{
const number = "57140189244892014"
const start_amount = 1876
    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
           done();
        });
    });

    it('should create a new AccountEvent',(done)=>{
        Account.create({account_nmbr:number,balance:start_amount},(err,result)=>{
            expect(err).to.be.null;
            AccountEvent.findOne({account:result._id}, (err,doc) =>{
                expect(doc).to.not.be.null;
                expect(doc.event_type).to.equal('created')
                expect(doc.amount).to.equal(start_amount)
                done();
            });
        });
    });

    after(done=>{
        mongoose.connection.close();
        done();
    })
});

describe('AccountEvent Validation', (done) =>{
    const number = 57140189244892014;
    const start_amount = 1876;
    let account_id;

    before((done) => {
        mongoose.connect('mongodb://localhost:27017/meccano_test');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',()=>{
            Account.create({account_nmbr:number, balance:start_amount},(err,result)=>{
                account_id =result._id
            })
            done();
        });
    });

    it('should reject save() on existing account events',(done)=>{
        AccountEvent.findOne({},(err,result)=>{
            result.balance = 345678;
            result.save((err,res)=>{
                expect(err).to.not.be.null;
                done();
            });
        })
    });

    it('should reject update() on existing account events',(done)=>{
        //weirdCar.update({$inc: {wheels:1}}, { w: 1 }, callback);
        AccountEvent.findOne({},(err,result)=>{
            result.update({amount:67867868},(err,res)=>{
                expect(err).to.not.be.null;
                done();
            });
        });

    });

    it('should reject remove() an account event',(done)=>{
        AccountEvent.findOne({},(err,result)=>{
            result.remove((err,res)=>{
                expect(err).to.not.be.null;
                done()
            });
        });
    });

    it('should reject updateOne() on existing account events',(done)=>{
        //weirdCar.update({$inc: {wheels:1}}, { w: 1 }, callback);
        AccountEvent.findOne({},(err,result)=>{
            result.update({amount:67867868},(err,res)=>{
                expect(err).to.not.be.null;
                done();
            });
        });

    });


    after(done=>{
        mongoose.connection.close();
        done();
    })
});