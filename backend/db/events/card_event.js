const ImmutableError = require('../../error_handling/ImmutableError')
const mongs = require('mongoose');

const cardEventSchema = mongs.Schema({
    card:{type:mongs.Schema.Types.ObjectId, ref:'Card', required:true},
    event_type: { type: String, enum: ['auth_success','auth_error', 'revoked','replaced','activated'], required: true },
    description:mongs.Schema.Types.Mixed,
    time:{type:Date, required:true}},
    {timestamp:{createdAt: 'created_at' }}
);
cardEventSchema.pre('save', function (next) {
    if(!this.isNew){
        throw ImmutableError();
    }
    next();
});
cardEventSchema.pre('update', function (next) {
        throw ImmutableError();
});
cardEventSchema.pre('delete', function (next) {
        throw ImmutableError();
});

const CardEvent= mongs.model('CardEvent',cardEventSchema,'card_events');
module.exports = CardEvent;