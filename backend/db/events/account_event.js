const ImmutableError = require('../../error_handling/ImmutableError')
const mongs = require('mongoose');

const accountEventSchema = mongs.Schema({
        account:{type:mongs.Schema.Types.ObjectId, ref:'Account', required:true},
        card:{type:mongs.Schema.Types.ObjectId, ref:'Card'},
        amount:{type:Number, required:true},
        event_type: { type: String, enum: ['created','withdraw','cancel','deposit'], required: true },
        description: mongs.Schema.Types.Mixed,
        time:{type:Date, required:true}},
       {timestamp:{createdAt: 'created_at' }}
);
accountEventSchema.pre('save', function (next) {
    if(!this.isNew){
        throw new ImmutableError("500","Attempt to update account event");
    }
    next();
});
accountEventSchema.pre('update', function (next) {
    throw new ImmutableError();
});
accountEventSchema.pre('remove', function (next) {
    throw new ImmutableError();
});

const AccountEvent= mongs.model('AccountEvent',accountEventSchema,'account_events');
module.exports = AccountEvent;