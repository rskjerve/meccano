const ImmutableError = require('../../error_handling/ImmutableError')
const OrderEnum =require('../enums').orderModel.event
const mongs = require('mongoose');

const orderEventSchema = mongs.Schema({
        order:{type:mongs.Schema.Types.ObjectId, ref:'Account', required:true},
        event_type: { type: String, enum: [OrderEnum.received,OrderEnum.request_sent,OrderEnum.rejected,OrderEnum.accepted], required: true },
        description: mongs.Schema.Types.Mixed,
        time:{type:Date, required:true}},
);

orderEventSchema.pre('save', function (next) {
    if(!this.isNew){
        throw new ImmutableError("500","Attempt to update order event");
    }
    next();
});
orderEventSchema.pre('update', function (next) {
    throw new ImmutableError();
});
orderEventSchema.pre('remove', function (next) {
    throw new ImmutableError();
});

const OrderEvent= mongs.model('OrderEvent',orderEventSchema,'order_events');
module.exports = OrderEvent;