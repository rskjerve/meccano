const Enums={}

Enums.cardModel = {
    event: {auth_success: 'auth_success',
            auth_error: 'auth_error',
            revoked: 'revoked',
            replaced:'replaced',
            activated: 'activated'}
        }

Enums.orderModel  ={
    status:{pending:'pending',canceled:'canceled',delivered:'delivered'},
    event:{received:'received',request_sent:'request_sent',rejected:'rejected',accepted:'accepted'},
    type:{visa:'visa',new_card:'new_card'}
}

module.exports = Enums;