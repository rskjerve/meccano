const CardCalculator={}
const mongoose = require('mongoose');
const mongodb =  "mongodb://localhost:27017/meccano";
const test_db = 'mongodb://localhost:27017/meccano_test';
const Card = require('./models/card');
const CardEvent = require('./events/card_event');
const _ = require('lodash');
const log = require('fancy-log')
const log_prefix='CardCalculator: ';

/**
 * POST /auth
 * @param cardNumber
 * @param pinCode
 * @param callback
 * @param test
 * @returns {Promise<any | never>} {status,action,message}
 */
CardCalculator.authentication = (cardNumber, pinCode, callback, test) =>{
    var connectTo = test? test_db : mongodb;
    mongoose.connect(connectTo)
    mongoose.Promise = global.Promise;
    db = mongoose.connection;
    db.on('error', ()=>{
        throw new DbError('connection issues');
    });

    return CardCalculator.findCard(cardNumber,pinCode)
     .then((result)=>{
            if(result.status === 400){
                CardCalculator.calculateError(result.card)
                    .then( (calc_result) => {
                       callback(calc_result);
                    });
            }
            else if (result.status === 200){
                CardCalculator.calculateSuccess(result.card._id)
                    .then((calc_result) =>{

                        if (calc_result.status === 200) {
                            calc_result.card = result.card.card_number;
                            calc_result.customer = result.card.customer.first_name + ' '+ result.card.customer.last_name;
                            calc_result.balance = result.card.account.balance;
                        }
                        callback(calc_result)}
                    )
            }
      }).catch(error =>{
                callback(error);
        });


}

/**
 * Attempts to find card with cardNumber and corresponding pin in db
 * @param cardNumber
 * @param pinCode
 * @returns {Promise<any>} {success}, {id/cardauth error}
 */
CardCalculator.findCard = (cardNumber,pinCode)=>{
    return new Promise(function(resolve,reject){
       Card.findOne({card_number:cardNumber,pin:pinCode}).
        populate('customer').
            populate('account','balance')
            .exec((err,doc)=>{
               if (err){
                   log.error(log_prefix + 'auth; cards could not be accessed');
                   reject({status: 500});
               }
               if (doc === null){
                   resolve({status:400, card:cardNumber});
               } else {resolve({status: 200, card:doc});}
           });
    });
}

/**
 * Calculates state when card was found
 * @param id
 * @param event_type
 * @returns {Promise<any>}
 */
CardCalculator.calculateSuccess = (id) => {
    return new Promise(function (resolve,reject) {
        //TODO sort desc date
        CardEvent.find({card: id},null,{sort:'+time'}, function(err, results){
            if(err) reject({status:500});
            const outcome = CardCalculator.replay(results,false);
            resolve(outcome);
        })
    })
}

/**
 *Calculates status when card was not found
 * @param id
 * @returns {Promise<any>}
 */
CardCalculator.calculateError = (id) => {
    return new Promise(function (resolve, reject) {
        Card.findOne({card_number: id}, (err, result) => {
            CardEvent.find({card: result._id},null,{sort:'+time'}, function (err, results) {
                if (err) reject({status: 500});
                const outcome = CardCalculator.replay(results,true)
                resolve(outcome);
            })
        });
    });
}

/**
 * Calculates current state
 * @param events
 * @returns {*}
 */

CardCalculator.replay = async (events,wasError) =>{
       var attempted_auths = 0;
       var response = {}
       const card_id = _.head(events).card
       _.forEach(events,(event)=>{
            switch (event.event_type) {
                case 'revoked':
                    response = {status:400, action:'revoke',message:"this card was revoked at " + event.createdAt};
                    return false;
                    break;
                case 'replaced':
                    response = {status:400, action:'revoke',message:"this card was replaced at " + event.createdAt};
                    return false;
                    break;
                case 'auth_error':
                    attempted_auths ++;
                    break;
                case "auth_success":
                    attempted_auths = 0;
                    break;
            }
       })

    if(!wasError && _.isEmpty(response)){
        await CardCalculator.writeSuccessEvent(card_id)
        return {status:200}
    }else if(wasError &&_.isEmpty(response)){
        attempted_auths ++;
        if(attempted_auths > 2){
            await CardCalculator.writeErrorEvent(card_id,'revoked','auth attempts limits reached ' + Date.now())
            Card.updateOne({card_id},{active:false});
            return {status:400, action:'revoke',message:" auth attempts limits has been reach, card was revoked at" + Date.now()};
        } else {
            await CardCalculator.writeErrorEvent(card_id, 'auth_error', 'wrong pin entered')
            return {status: 400, action: 're-enter', attempts: attempted_auths}
        }
    }else {
        await CardCalculator.writeErrorEvent(card_id,'revoked','attempt to use revoked card at '+ Date.now());
        Card.updateOne({_id:card_id},{active:false});
        return response;
    }
}

/**
 * Writes events to event store when auth fails
 * @param card_id
 * @returns {Promise<any>}
 * TODO error handling
 */

CardCalculator.writeErrorEvent =(card_id,event_type,description) =>{
    CardEvent.create({card: card_id, time:Date.now(),event_type:event_type, description:description},(err,res)=>{
        if(err){
            log.error(log_prefix+"could not create event" +event_type+" to "+ card_id + "at" + Date.now(), err)
            return false;
        } else{
           log.info(log_prefix+ event_type+" written to "+ res.card + " event store at" + res.time+'\n');
           return true
        }
    });
};

/**
 * Writes events to event store when auth succeds
 * @param card_id
 * @returns {Promise<any>}
 */
CardCalculator.writeSuccessEvent = async (card_id) =>{
        CardEvent.create({card: card_id, time: Date.now(),event_type:'auth_success'},(err,res)=>{
            if(err){
                log.error(log_prefix+"could not create event" +' auth_success '+" to "+ card_id,err)
                throw (err);
            }
            log.info(log_prefix + "auth suceess event written to event store for card " +card_id+ " at " + res.time)
        });
};


module.exports = CardCalculator;