const mongs = require('mongoose')
const AccountEvent = require('../events/account_event')

const accountSchema = mongs.Schema({
    account_nmbr:{type: Number, required:true},
    balance: {type:Number, required: true}
});

accountSchema.pre('save', function (next) {
    if(this.isNew){
        AccountEvent.create({account:this._id,amount:this.balance,event_type:'created',time:Date.now()},
            (err,result)=>{
                if(err) throw err;
                next();
            })
    }
    next();
});
const Account = mongs.model('Account', accountSchema, 'accounts');



module.exports = Account;