const mongs = require('mongoose')

const customerSchema = mongs.Schema({
    first_name:String,
    last_name: String,
    address: {street_name:String,street_nmbr:String,postalCode:Number, area:String},
    phone:Number,
    email:String,
    account:{type: mongs.Schema.Types.ObjectId, ref:'Account'}
});
const Customer = mongs.model('Customer', customerSchema, 'customers');

module.exports = Customer;