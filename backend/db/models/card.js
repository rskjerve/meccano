//card + card event
const mongs = require('mongoose')
const CardEvent =require('../events/card_event')

const cardSchema =  mongs.Schema({
    card_number:String,
    pin: Number,
    account:{type: mongs.Schema.Types.ObjectId, ref: 'Account'},
    customer: {type: mongs.Schema.Types.ObjectId, ref: 'Customer'},
    active:{type: Boolean, default:true}
})
cardSchema.pre('save',function (next) {
    if(this.isNew) {
        CardEvent.create({card: this._id, event_type: 'activated', time: Date.now()},(err,result)=>{
            if (err) throw err;
            next();
        })
    }
})

cardSchema.statics.getAccount = function(number) {
    const account = this.findOne({ card_number: number},null,{select:'+account'});
   return account;
};

const Card = mongs.model('Card', cardSchema, 'cards');
module.exports = Card;
