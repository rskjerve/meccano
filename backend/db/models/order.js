const mongs = require('mongoose')
const Card = require('./card')
const OrderEvent = require('../events/order_event')
const OrderEnum =require('../enums').orderModel.status;
const OrderEnumType =require('../enums').orderModel.type;


const orderSchema = mongs.Schema({
    card:{type: mongs.Schema.ObjectId, ref:'Card', required:true},
    amount: {type:Number},
    order_type: {type:String, enum:[OrderEnumType.visa,OrderEnumType.new_card]},
    status:{type:String, enum:[OrderEnum.pending,OrderEnum.canceled,OrderEnum.delivered]}
});

orderSchema.pre('save', function (next) {
    this.order_type = this.amount? OrderEnumType.visa :OrderEnumType.new_card;
    if(this.isNew){
        this.status='pending';
        OrderEvent.create({order:this._id,event_type:'received',time:Date.now()},
            (err,result)=>{
                if(err) throw err;
                if(!result)throw err;
                next();
            })
    }else {
        next();
    }
});
orderSchema.statics.updateStatus = function(order_id,newStatus,callback) {
    this.findOneAndUpdate({_id: order_id},{status:newStatus},function (err,result) {
        callback(err,result);
    });
};


const Order = mongs.model('Order', orderSchema, 'orders');

module.exports = Order;