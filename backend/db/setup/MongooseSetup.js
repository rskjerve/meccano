const mongoose = require('mongoose');
const mongodb =  "mongodb://localhost:27017/meccano";
const log_prefix ="Mongoose setup: "
const log = require('fancy-log');
const Account = require('../models/account')
const Customer = require('../models/customer')
const Card = require('../models/card');
const CardEvent= require('../events/card_event')
let db = null;
let cards_added =0;
const _ =  require('lodash');


const MongooseSetup={}

MongooseSetup.start = function () {
    mongoose.connect(mongodb);
    mongoose.Promise = global.Promise;
    db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    MongooseSetup.fill();

}

MongooseSetup.fill= function(){
    Customer.findOne({},(err,res)=>{
        if (res == null){
            MongooseSetup.addCustomers();
        } else {
            log(log_prefix, "customers already exist")
        }
    })
}


MongooseSetup.addCustomers = ()=>{
    const customer =[{first_name: "Rowena", last_name:"Sprout", address :{street_name: "Grimmauld Place",
                    street_nmbr: "12", postalCode: 12237, area:"London"}},
                {first_name: "Akira", last_name:" Gallagher", address :{street_name: "Rockford Mountain Lane",
                    street_nmbr: "361", postalCode: 54901, area:"Oshkosh"}},
                {first_name: "Richard", last_name:"Alvarado", address :{street_name: "Marvolo street",
                     street_nmbr: "1", postalCode: 54901, area:"Liitle Hangleton"}},
                {first_name: "Hayden", last_name:"Gage", address :{street_name: "Whitehaven Mansions",
                     street_nmbr: "56B", postalCode: 19937, area:"London"}}
                ]

    Customer.insertMany(customer,(err,result)=>{
        if(err) throw err;
        MongooseSetup.addAccounts(result)
    })
}

MongooseSetup.addAccounts =  function (customers) {
    log.info(log_prefix,"Customers were added")
    _.forEach(customers, function (value) {
        switch(value.first_name){
            case 'Rowena':
                Account.create({account_nmbr:15439998288,balance: 16893}, (err,result)=>{
                    if(err) throw err;
                    console.log('account created:',result)
                    MongooseSetup.addCards(result._id,value._id,'Rowena');
                })
                break;
            case 'Akira':
                Account.create({account_nmbr:15889268288, balance: 29008}, (err,result)=>{
                    if(err) throw err;
                    MongooseSetup.addCards(result._id,value._id,'Akira');
                })
                break;
            case 'Richard':
                Account.create({account_nmbr:91739466672, balance: 2008}, (err,result)=>{
                    if(err) throw err;
                    MongooseSetup.addCards(result._id,value._id,'Richard');
                })
                break;
            case 'Hayden':
                Account.create({account_nmbr:15889268281,balance: 208}, (err,result)=>{
                    if(err) throw err;
                    MongooseSetup.addCards(result._id,value._id,'Hayden');
                })
                break;
        }
    });
}

MongooseSetup.reset = ()=>{
    mongoose.connection.db.dropDatabase(function(err, result) {log_prefix,log.info('Database was dropped')});
}

MongooseSetup.addCards = (accountId, customerId,customerName) =>{
    log.info(log_prefix,"Account was added for ",customerName)
    switch(customerName){
        case 'Rowena':
            console.log(customerId)
            Card.create({card_number: "432574635211",account:accountId,customer: customerId,pin: "6475"})
                .then(result => MongooseSetup.cardAdded(result));
            break;
        case 'Akira':
            Card.create({card_number: "432574905233",account:accountId,customer: customerId,pin: "9182"})
                .then(result => MongooseSetup.cardAdded(result));
            break;
        case 'Richard':
            Card.create({card_number: "481475935081",account:accountId,customer: customerId,pin: "3329"})
                .then(result => MongooseSetup.cardAdded(result));
            break;
        case 'Hayden':
            Card.create({card_number: "281498735081",account:accountId,customer: customerId,pin: "4385"})
                .then(result => MongooseSetup.cardAdded(result));
            break;
    }
}

MongooseSetup.cardAdded = (added) =>{
    cards_added++;
    if(cards_added === 4){
        log.info('MongooseSetup completed')
    }
}
module.exports = MongooseSetup;