const AccountCalculator ={};
const Account = require('./models/account');
const AccountEvent = require('./events/account_event')
const Error = require('../error')
const _ = require('lodash');

AccountCalculator.withdraw =(account_id,withdrawAmount,description)=>{
    return new Promise(function (resolve, reject) {
        if(!account_id){
            reject({message:'account id missing'})
        }
        if(!withdrawAmount){
            reject({message:'amount missing'})
        }

        withdrawAmount =_.toSafeInteger(-withdrawAmount);
        const date =Date.now()

        AccountEvent.create({account: account_id,event_type:'withdraw',amount:withdrawAmount,time:date,description:description},
            (err,doc)=>{
                if(err){reject(err)}
                 AccountCalculator.calculate(doc.account)
                    .then(result=>{
                        resolve({status:200,balance:result});
                    })
                    .catch(err =>{
                        if(err.status==400){
                        AccountCalculator.cancel(account_id,_.toSafeInteger(-withdrawAmount),
                            {reason:Error.withdraw_insufficent})
                            .then(result => {
                                reject(result)                             }
                            )
                            .catch(err =>{
                                reject(err)
                            });
                        }
                    });
            });
    });
}

AccountCalculator.deposit=(account_id,depositAmount)=>{
    return new Promise(function (resolve, reject) {
        const date =Date.now()
        if(depositAmount < 0){
            reject({status:400, message:'cannot deposit negative amount'})
        }
        AccountEvent.create({account: account_id,event_type:'deposit',amount:depositAmount,time:date},
            (err,doc)=>{
                if(err){reject(err)}
                AccountCalculator.calculate(doc.account)
                    .then(result=>{
                        resolve({status:200,balance:result});
                    })
                    .catch(err =>{
                        reject({status:500,message:'Server error on deposit attempt'});
                    })
            });
    });
}

AccountCalculator.cancel=(account_id,originalAmount,description)=>{
    return new Promise(function (resolve, reject) {
        const date =Date.now()
        if(!description){
            reject({status:0,message:'AccountEvents of type "cancel" must include a description'})
        }

        cancelAmount =_.toSafeInteger(-originalAmount);

        AccountEvent.create({account: account_id,event_type:'cancel',amount:originalAmount,time:date},
            (err,doc)=>{
                if(err){reject(err)}
                AccountCalculator.calculate(doc.account)
                    .then(result=>{
                        resolve({status:400,message:'Insufficent funds, transaction was canceled',reason:Error.withdraw_insufficent,
                                balance:result});
                    })
                    .catch(err =>{
                        reject({status:500,message:'Server error on cancelling attempt'});
                    })
            });
    });
}

AccountCalculator.calculate =(account_id)=>{
    return new Promise((resolve,reject) => {
        AccountCalculator.getAll(account_id)
           .then((result)=>{
                var newBalance=0;
                _.each(result,(event)=>{
                    newBalance = newBalance+event.amount;
                });
                if(newBalance < 0){
                    reject({status:400})
                } else {
                    AccountCalculator.updateAccountBalance(account_id,newBalance)
                        .then(resolve(newBalance))
                        .catch(error => reject(error));
                };
           });
    });
}

AccountCalculator.getAll= (account_id) => {
    return new Promise((resolve => {
        AccountEvent.find({account:account_id}, null,{sort:'+time'},(err,result)=>{
           if(err){reject(err)}
           resolve(result);
        });
    }));
};

AccountCalculator.updateAccountBalance = (account_id,newBalance)=>{
    return new Promise((resolve, reject) => {
        Account.updateOne({_id:account_id},{balance:newBalance},(err,result)=>{
           if(err !== null) reject(err);
           resolve(result);
        });
    });
};


module.exports = AccountCalculator;