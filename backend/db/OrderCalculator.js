const Order = require('./models/order');
const OrderEvent = require('./events/order_event');
const OrderEnums =require('./enums').orderModel;
const OrderEventEnums =OrderEnums.event;

const AccountCalculator = require('./AccountCalculator');
const Card = require('./models/card');
const DummyServices = require('./DummyServices');
const Error = require('../error');
const Success =require('../success');
const _ = require('lodash');

const OrderCalculator={};

OrderCalculator.visa =(card_number,amount)=>{
    return new Promise(function (resolve, reject) {
        OrderCalculator.getCardId(card_number)
            .then(result=>{
                Order.create({card:result._id,amount:amount},(err,result)=>{
                    if(err){reject (err)}
                    else if (!result){
                        reject(Error.visa_server_error);
                    }
                    else{
                        const order_id = result._id;
                        OrderCalculator.updateBalance(card_number,result._id,amount)
                            .then(result=>{
                                resolve(result);
                            })
                            .catch(err=>{
                                reject(err);
                        });

                    };
                })
            })
    });
}

OrderCalculator.getCardId=(card_number)=>{
    return new Promise(function (resolve,reject) {
        Card.findOne({card_number:card_number},(err,result)=>{
            if(err){
                reject(Error.card_server_error);
            } else {
                if(result==null){
                    reject(Error.card_server_error)
                }
                resolve(result);
            }
        });
    });
}



OrderCalculator.updateBalance= (card_number,order_id,amount)=> {
    return new Promise(function(resolve,reject){
        Card.getAccount(card_number)
            .then(result=>{
                AccountCalculator.withdraw(result.account,amount,{purchase:'visa gift card',order:order_id})
                    .then(result=>{
                        const balance = result.balance;
                        if(!result){reject(Error.withdraw_unspecified)}
                        else {
                         OrderCalculator.placeOrder(order_id)
                             .then(result=>{
                                 let response = (Success.setBalanceAndId(Success.visa_ok,order_id,balance));
                                 resolve(response);
                             });
                        }
                    })
                    .catch(error=>{
                        OrderCalculator.cancelOrder(order_id,Error.withdraw_insufficent)
                            .then(result=>{
                                let response = Error.setId(Error.withdraw_insufficent,order_id);
                                reject(response)});
                   });

            })
            .catch(err=> reject(Error.card_server_error));
    });
}

OrderCalculator.cancelOrder = (order_id,description)=>{
    return new Promise(function (resolve) {
        OrderEvent.create({
            order: order_id, event_type: OrderEventEnums.rejected,
            description: description, time: Date.now()}, (err, result) => {
            let response = Error.withdraw_insufficent;
            response.id = order_id;
            OrderCalculator.updateStatus(order_id)
                .then(result=>{
                    resolve(result);
                })
        });
    });
}

OrderCalculator.placeOrder = (order_id,description)=>{
    return new Promise(function(resolve,reject) {
        const accepted = DummyServices.orderVisa();
        OrderEvent.create({order:order_id,event_type: OrderEventEnums.request_sent,description: description,time:Date.now()},
            (err,result)=>{
                if(accepted){
                    OrderCalculator.handleAccepted(order_id)
                        .then(result=>resolve(result))
                }
                else {
                    OrderCalculator.cancelOrder(order_id,Error.visa_server_error)
                        .then(result=>reject(result))

                }

            });

    });
}

OrderCalculator.handleAccepted = (order_id)=>{
    return new Promise(resolve => {
        OrderEvent.create({order: order_id, event_type: OrderEventEnums.accepted, time: Date.now()},
            (err, result) => {
                OrderCalculator.updateStatus(order_id)
                    .then(result=>{
                        resolve(result);
                    });
            });
    });
}

OrderCalculator.updateStatus = (order_id)=> {
    return new Promise((resolve, reject) => {
        OrderEvent.find({order: order_id}, null, {sort: '+time'}).exec((err, result) => {
                var replay=OrderEventEnums.received;
                _.each(result, res_doc => {
                    replay = res_doc.event_type;
                });
                1+1;
                if (replay === OrderEventEnums.rejected) {
                    Order.updateStatus(order_id, OrderEnums.status.canceled, (err, result) => {
                        if (err) reject(Error.card_server_error);
                        else resolve(Success.order_update_ok)
                    })
                }
                else if (replay == OrderEventEnums.accepted) {
                    Order.findById(order_id,(err,result)=>{
                        result.status=OrderEnums.status.delivered;
                        result.save((err,result)=>{
                            resolve(Success.order_update_ok)
                        })
                    })
                }
                else {
                    resolve(Success.order_update_ok);
                }
        });
    });
};

module.exports=OrderCalculator;
