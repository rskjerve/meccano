import React from 'react'
export const CustomerContext = React.createContext();

export function withCustomerContext(Component) {
    return function CustomerComponent(props) {
        return (
            <CustomerContext.Consumer>
                {(contexts) => <Component {...props} {...contexts} />
                }
            </CustomerContext.Consumer>
        )
    }
}
