import React, {Component} from 'react'
import Button from '@material-ui/core/Button'
import './Transactions.css'


class TransactionReceipt extends Component{
    render(){
        return (
            <div className="TransactionReceipt">
                <h2>Do you want a receipt?</h2>
                <div id ="receiptButtons" className="DuoButtonBox">
                    <Button variant="contained" onClick={()=>this.props.onClick(true)}>Yes</Button>
                    <Button variant="contained" onClick={()=>this.props.onClick(false)}>No</Button>
                </div>
            </div>

        )
    }
}

export default TransactionReceipt;