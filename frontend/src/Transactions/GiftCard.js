import React, {Component} from 'react';
import '../Navigation/Nav.css'
import AmountPicker from "./AmountPicker/AmountPicker";

class GiftCard extends Component{
    render(){
        return(
            <div>
                <h1>Choose amount for the giftcard</h1>
                <AmountPicker/>
            </div>
        );
    }
}

export default GiftCard;
