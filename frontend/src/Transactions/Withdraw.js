import React, {Component} from 'react';
import AmountPicker from './AmountPicker/AmountPicker'

class Withdraw extends Component{

    render(){
        return(
            <div>
            <h1>Choose amount to withdraw</h1>
                <AmountPicker/>
            </div>
        );
    }
}

export default Withdraw;
