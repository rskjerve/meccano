import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

import Button from '@material-ui/core/Button';
import AmountTextField from "./AmountTextField";
import '../Transactions.css'

class AmountPicker extends Component{
    constructor(props){
        super(props);
        this.state= {
            other:false
        }
        this.handleOtherClicked = this.handleOtherClicked.bind(this);
        this.handleAmountClicked = this.handleAmountClicked.bind(this);
    }

    handleOtherClicked(showTextField){
        this.setState({'other':showTextField});
    }
    handleAmountClicked(amount){
        this.props.history.push(this.props.history.location.pathname + '/confirm?amount='+amount);
    }


    render(){
        return(
            <div id = "amount" className="amountPicker">
                <div id="amountColLeft" className="amountColumn">
                    <Button variant="outlined" onClick={()=>this.handleAmountClicked(200)}>200</Button>
                    <Button variant="outlined" onClick={()=>this.handleAmountClicked(400)}>400</Button>
                    <Button variant="outlined" onClick={()=>this.handleAmountClicked(500)}>500</Button>
                </div>
                <div id = "amountColRight" className="amountColumn">
                    {!this.state.other &&
                    <Button variant="outlined" id="otherAmountBtn" onClick ={()=>this.handleOtherClicked(true)} >Other</Button>
                    }
                    {this.state.other &&
                    <AmountTextField handleSubmit={this.handleAmountClicked} handleCancel={this.handleOtherClicked}></AmountTextField>
                    }

                    <Button variant="outlined" onClick={()=>this.handleAmountClicked(1000)}>1000</Button>
                    <Button variant="outlined" onClick={()=>this.handleAmountClicked(2000)}>2000</Button>
                </div>

            </div>
        )
    }
}

export default withRouter(AmountPicker);