import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import InputAdornment from'@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Check from '@material-ui/icons/Check';
import Cancel from '@material-ui/icons/Cancel';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    largeIcon: {
        fontSize: "0.9em"
    },
    lessMargin: {
        marginBottom:"10px"
    }
});

class AmountTextField extends React.Component {

    constructor(props){
        super(props);
        this.state={
            input:0,
            isActive:false
        }
        this.handleChange=this.handleChange.bind(this)
    }


    handleChange = input => event => {
        const value = event.target.value;
        if(value > 5000){
            event.target.value=5000;
        }
        else if(value > 99 && value < 200){
            event.target.value=200;
        }
        else if (value > 200 && value%100!==0){

            event.target.value = Math.round(value / 100)*100
        }


        this.setState({
            [input]: event.target.value,
            isActive:true
        });
    };

    render() {
        const { classes } = this.props;

        return (
            <FormControl id="otherAmountForm" className={classNames(classes.margin, classes.textField)}>
                <InputLabel htmlFor="CustomAmountTxt">Enter amount (minimum 200)</InputLabel>
                <Input
                    id="otherAmountInput"
                    type="number"
                    defaultValue={0}
                    vaule={this.state.input}
                    onChange={this.handleChange('input')}
                    inputProps={{min:200, max:5000, step:100}}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                className="amountTextBtn"
                                id="otherAmountCancel"
                                aria-label="Cancel"
                                onClick={()=>this.props.handleCancel(false)}
                                >
                                <Cancel className={classes.largeIcon}/>
                            </IconButton>
                            {(this.state.isActive && this.state.input > 199) &&
                                <IconButton
                                    className="amountTextBtn"
                                    id="otherAmountSubmit"
                                    aria-label="Submit" disabled={this.state.input < 200}
                                    onClick={() => this.props.handleSubmit(this.state.input)}
                                >
                                    <Check className={classes.largeIcon}/>
                                </IconButton>
                            }
                        </InputAdornment>
                    }
                />
            </FormControl>
        );
    }
}

AmountTextField.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AmountTextField);
