import React, {Component} from 'react';
import {withRouter} from 'react-router-dom'

import _ from 'lodash'
import AmountPickerService from './AmountPickerService'
import {withCustomerContext} from "../../Customer";

import Button from '@material-ui/core/Button';
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";


class AmountConfirm extends  Component{
    constructor(props){
        super(props);
        this.state={
            amount:_.split(this.props.history.location.search,'=')[1],
            feature: _.split(this.props.history.location.pathname,'/')[1],
        }
        this.handleOkClicked=this.handleOkClicked.bind(this);
        this.handleSuccess = this.handleSuccess.bind(this);
        this.handleError=this.handleError.bind(this);
    }

    handleOkClicked(){
      this.setState({'pending':true})
      AmountPickerService.postAmountRequest(this.props.history.location.pathname,this.state.amount
                                            ,{success:this.handleSuccess, error:this.handleError});
    }

    handleSuccess(data){
        this.props.updateBalance(data.data.balance)
        this.props.history.push('/'+this.state.feature+'/result?success='+data.data.message);
    }

    handleError(error){
        this.props.history.push('/'+this.state.feature+'/result?error='+error.data.message);
    }

    render(){
        const feature_headings={
            "withdraw":"Confirm withdrawal",
            "gift": "Confirm gift card purchase"
        }
        const feature_class={
            "withdraw":"move50Left",
            "gift": "keepPosition"
        }

        return(
        <div id="amountConfirm" className={feature_class[this.state.feature]}>
            <h1>{feature_headings[this.state.feature]}</h1>
            <h2>{"Amount: "+this.state.amount+" "+ this.props.currency}</h2>
            {this.state.pending &&
                <div id="amountProgress">
                <CircularProgress/>
                </div>
            }
          {!this.state.pending &&
            <div className="DuoButtonBox">
                <Button onClick={this.props.history.goBack} variant="contained">
                    <img src={require('./../../img/cancel.png')} alt=""/>
                    Cancel
                </Button>
                <Button onClick={this.handleOkClicked} variant="contained">
                    <img src={require('./../../img/ok.png')} alt="" />
                    OK
                </Button>
            </div>
            }
        </div>)
    }
}
export default withCustomerContext(withRouter(AmountConfirm));