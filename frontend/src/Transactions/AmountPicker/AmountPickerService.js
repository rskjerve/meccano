import _ from 'lodash';
import Request from '../../DataService'

class AmountPickerService {

    static postAmountRequest(fromLocation,confirmedAmount,callback){
        const data = _.words(fromLocation)
        const req = new Request()
        req.apiPost({amount:confirmedAmount},data[0],callback)
    }
}

export default AmountPickerService;