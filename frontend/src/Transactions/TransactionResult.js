import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import TransactionReceipt from './TransactionReceipt'
import Button from '@material-ui/core/Button'
import _ from "lodash";
import LogoutDialog from "../Auth/LogoutDialog";
import '../style/App.css'
import './Transactions.css'


class TransactionResult extends Component{

    constructor(props){
        super(props);
        this.state = {
            reason:_.split(this.props.history.location.search,'=')[1],
            status:_.split(this.props.history.location.search,/[=?]/)[1],
            feature: _.split(this.props.history.location.pathname,'/')[1],
            showReceiptOpt:true,
            withReceipt:false,
            output:false,
            logout:false,
            timer:10
        }

        this.handleRecepitOpt=this.handleRecepitOpt.bind(this);
        this.handleLogout=this.handleLogout.bind(this);
    }

    handleRecepitOpt(withRecepit) {
        this.setState({'withReceipt': withRecepit, 'showReceiptOpt': false, output: true,status:false})
    }

    handleLogout(){
        this.setState({logout:true})
    }

    startCountdown(){
        let i = 0
        this.timerId= setInterval(()=>{
            i=i+1
            if (i === 10){
                    this.props.onLogout();
                }
            else {
                this.setState({timer:(10-i)});
            }
            },
            1000);
    }

    componentDidMount(){
        const reason = _.split(this.props.history.location.search,'=')[1];
        console.log(reason)
        if(_.isEqual(reason, 'token expired') || _.isEqual(reason,'token%20expired')){
            this.startCountdown();
        }
    }

    componentWillUnmount(){
        clearInterval(this.timerId);
    }


    render(){
        const feature_headings={
            "withdraw":"Withdraw was successful",
            "gift": "Visa gift card purchased"
        }

        const feature_decoration={
            "withdraw":<img src={require("../img/money.png")} alt="drawing of paper money"/>,
            "gift":<img src={require("../img/result-gift.png")} alt="drawing of gift"/>,
            "output":<img src={require("../img/cash-in-hand.png")} alt="drawing of cash in hand"/>,

        }


        const feature_error_headings={
            "withdraw":"Could not withdraw money",
            "gift": "Could not purchase gift card"
        }

        const feature_output = {
            "withdraw":"Take your money from the tray",
            "gift": "Take your gift card from the tray"
        }


        return(
        <div className="TransactionResult">
            {_.isEqual(this.state.status,'success') &&
                <div>
                    <h1>{feature_headings[this.state.feature]}</h1>
                    <div className="resultDecoration">
                    {feature_decoration[this.state.feature]}
                    </div>
                    {this.state.showReceiptOpt &&
                        <TransactionReceipt onClick={this.handleRecepitOpt}/>
                    }

                </div>
            }
            {_.isEqual(this.state.status,'error') &&
            <div>
                <h1>{feature_error_headings[this.state.feature]}</h1>
                <div id="errorDecoration" className="resultDecoration">
                    <img src={require('../img/error-64.png')} alt="warning icon"/>
                </div>

                    {(_.isEqual(this.state.reason, 'token expired') || _.isEqual(this.state.reason,'token%20expired'))
                     &&
                    <div className="move30right">
                        <h2 id="resultTokenExpiredText">{"Session expired. You will be logged out in "+this.state.timer+" seconds"}</h2>
                    </div>
                    }
                    {!(_.isEqual(this.state.reason, 'token expired') || _.isEqual(this.state.reason,'token%20expired'))
                    &&
                        <div className="move50Left">
                            <h2 id="resultErrorText">{this.state.reason}</h2>
                        </div>
                    }

                <div id="resultFailedButtons" className="DuoButtonBox">
                    <Button onClick={this.handleLogout} variant="contained">Logout</Button>
                    <Button onClick={()=> this.props.history.push('/')} variant="contained">Back to menu</Button>
                </div>
            </div>
            }
            { (this.state.output )&&
                <div>
                    {this.state.withReceipt &&
                        <p id ="ReceiptPrinting"> Your receipt is being printed</p>
                    }
                    <h1>{feature_output[this.state.feature]}</h1>

                    <div className="resultDecoration">
                        {feature_decoration["output"]}
                    </div>
                    <div id="resultOkButtons" className="DuoButtonBox">
                        <Button variant="contained" onClick={this.handleLogout}>Logout</Button>
                        <Button variant="contained" onClick={()=> this.props.history.push('/')}>Back to menu</Button>
                    </div>
                </div>
            }

         <LogoutDialog open={this.state.logout} onLogout={this.props.onLogout} onCancel={()=> this.props.history.push('/')}/>

        </div>
        )
    }
}

export default withRouter(TransactionResult);

