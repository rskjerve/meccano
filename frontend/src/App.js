import React, { Component } from 'react';
import './style/App.css';

import {Switch , Route, withRouter} from 'react-router-dom'
import NavBar from "./Navigation/NavBar";
import BalanceBox from "./BalanceBox";
import Home from "./Navigation/Home";
import Withdraw from "./Transactions/Withdraw";
import GiftCard from "./Transactions/GiftCard"
import Cards from "./Transactions/Cards";
import Profile from "./Transactions/Profile";
import Breadcrumb from "./Breadcrumb";
import Auth from "./Auth/Auth";
import Logout from "./Auth/Logout"
import AmountConfirm from "./Transactions/AmountPicker/AmountConfirm";
import TransactionResult from './Transactions/TransactionResult';

import DataService from "./DataService"
import {CustomerContext} from "./Customer";

class App extends Component {
   constructor(props){
       super(props)
       const token = localStorage.getItem('token');
       const customer = localStorage.getItem('customer')
       const balance = localStorage.getItem('balance')
       this.state = {
           token:token,
           customer: customer,
           balance:balance,
           currency: "NOK",
           logout:false
       }
       this.handleAuthSuccess= this.handleAuthSuccess.bind(this);
       this.handleLogout = this.handleLogout.bind(this);
       this.updateBalance = this.updateBalance.bind(this);
       this.updateBalance = this.updateBalance.bind(this);
   }

   handleAuthSuccess(data){
       localStorage.setItem('token', data.token);
       const customerData =DataService.getCustomerFromToken(data.token)
       localStorage.setItem('customer',customerData.name);
       localStorage.setItem('balance',customerData.balance);
       this.setState({'token': data.token,'customer':customerData.name,balance:customerData.balance});
   }

   updateBalance(newBalance){
       localStorage.setItem('balance',newBalance);
       this.setState({balance:newBalance})
   }


   handleLogout(){
       this.setState({token:false});
       localStorage.removeItem('token');
       this.props.history.push('/')
   }

  render() {

      if (!this.state.token) {
          return <Auth onSuccess={this.handleAuthSuccess}/>;
      }

    return (
      <div className="App">
          <CustomerContext.Provider value = {this.state}>
              <a href={"#main_content"} className="SkipLink">Skip to main content</a>
              <NavBar/>
              <Breadcrumb/>
              <BalanceBox/>
          <main id="main_content">
              <Switch>
                  <Route exact path='/' component={Home}/>
                  <Route exact path='/withdraw' component={Withdraw}/>
                  <Route exact path='/withdraw/confirm' render = {props=> <AmountConfirm updateBalance={this.updateBalance}/>}/>
                  <Route exact path='/withdraw/result' render = {props => <TransactionResult onLogout={this.handleLogout}/>}/>
                  <Route exact path='/gift' component={GiftCard}/>
                  <Route exact path='/gift/confirm' render = {props=> <AmountConfirm updateBalance={this.updateBalance}/>}/>
                  <Route exact path='/gift/result' render = {props => <TransactionResult onLogout={this.handleLogout}/>}/>
                  <Route exact path='/card' component={Cards}/>
                  <Route exact path='/profile' component={Profile}/>
                  <Route exact path='/logout' render={props => <Logout handleOkClick={this.handleLogout}/>}/>
              </Switch>
          </main>
          </CustomerContext.Provider>
      </div>);
  }
}

export default withRouter(App);
