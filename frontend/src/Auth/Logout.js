import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import LogoutDialog from './LogoutDialog'

class Logout extends Component{
    constructor(props){
        super(props)
        this.state={
            open:true
        }
    }
    render(){
        return(
            <div>
                <LogoutDialog open={true} onLogout={this.props.handleOkClick} onCancel={()=> this.props.history.push('/')}/>
            </div>
        )
    }
}

export default withRouter(Logout);