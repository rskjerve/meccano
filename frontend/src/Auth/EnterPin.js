import React from 'react';
import Grid from 'react-css-grid';
import './Auth.css';
import EnterPinColumns from './EnterPinColumns'
import WrongPinDialog from './WrongPinDialog'
import RevokedCardDialog from './RevokedCardDialog'
import Request from "../DataService";
import CircularProgress from '@material-ui/core/CircularProgress'
import _ from 'lodash'

class EnterPin extends React.Component{

    constructor(props){
        super(props);
        this.state={
            input:[],
            disableDelete:true,
            disableOK:true,
            errorMessage:null,
            errorDialog:false,
            attempts:0,
            revoked:false,
            waiting:false
        }
        this.handleNumpadClick = this.handleNumpadClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleOkClick = this.handleOkClick.bind(this);
        this.handleAuthError = this.handleAuthError.bind(this);
        this.handleDialogClose = this.handleDialogClose.bind(this);
    }

    handleNumpadClick(number){

        if(this.state.input.length < 4) {
            const newInput = _.clone(this.state.input);
            newInput.push(number);
            this.setState({'input': newInput},()=>{
                const newLength = this.state.input.length;
                switch (newLength) {
                    case 1:
                        this.setState({disableDelete:false});
                        break;
                    case 4:
                        this.setState({disableOK:false});
                        break;
                    default:
                        break;
                }
            });
        }
    }

    handleDeleteClick(){
        let newInput =_.clone(this.state.input);
        newInput.pop();
        this.setState({'input':newInput},()=>{
            const newLength = this.state.input.length;
            switch (newLength) {
                case 0: this.setState({disableDelete:true})
                    break;
                case 3:this.setState({disableOK:true})
                    break;
                default:
                    break;
            }
            console.log(this.state.input)
        });
    }

    handleOkClick(){
        const req = new Request()
        const card = 432574635211
        const pin =_.join(this.state.input, '');
        this.setState({'waiting':true},()=>{
            req.apiAuth(card,pin,{success:this.props.onSuccess, error:this.handleAuthError});
        });
    }

    handleAuthError(status){
        if(status.action==='re-enter'){
            console.log('re-enter')
            this.setState((state) => {
                return {errorDialog:true,
                        input: [],
                        disableDelete:true,
                        disableOK:true,
                        attempts: status.attempts,
                        waiting:false
                };
            });
        } else if (status.action === 'revoke'){
            this.setState({'revoked':true,'waiting':false});
        } else {
        }
    }

    handleDialogClose(){
        this.setState({'errorDialog':false})
    }
    render(){
        return(
            <div id="enterPinContainer" className="authActionsContainer">
            <h2 id="enterPinHeading"> Enter pin </h2>
                {this.state.waiting &&
                    <CircularProgress/>
                }
                <h3>
                    <span className={this.state.input.length>0?"activeNumberMarker":"inactiveNumberMarker"}>
                        *
                    </span>
                    <span className={this.state.input.length>1?"activeNumberMarker":"inactiveNumberMarker"}>
                        *
                    </span>
                    <span className={this.state.input.length>2?"activeNumberMarker":"inactiveNumberMarker"}>
                        *
                    </span>
                    <span className={this.state.input.length>3?"activeNumberMarker":"inactiveNumberMarker"}>
                        *
                    </span>
                </h3>
                <Grid id="pinGrid" gap={5} width={100}>
                    <EnterPinColumns id="PinColumnCancel" rows={[1,4,7,"Cancel"]} onNumber={this.handleNumpadClick}
                    onOption={this.props.onCancel}/>
                    <EnterPinColumns id="PinColumnDelete" rows={[2,5,8,"Delete"]} onNumber={this.handleNumpadClick}
                                     disableLast={this.state.disableDelete} onOption={this.handleDeleteClick}/>
                    <EnterPinColumns id="PinColumnOK" rows={[3,6,9,"OK"]} onNumber={this.handleNumpadClick}
                                     disableLast={this.state.disableOK} onOption={this.handleOkClick}/>
                </Grid>
                <WrongPinDialog onClose={this.handleDialogClose} onCancel={this.props.onCancel} open={this.state.errorDialog} attempts={this.state.attempts}/>
                <RevokedCardDialog onClose = {this.props.onCancel} open={this.state.revoked}/>
            </div>
        )
    }

}


export default EnterPin;

