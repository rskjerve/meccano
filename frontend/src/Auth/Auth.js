import React from 'react';
import EnterPin from "./EnterPin";
import './Auth.css'


class Auth extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            card_inserted:false,
            card_revoked:false
        }

        this.cardInserted = this.cardInserted.bind(this);
        this.handleCancelAuth = this.handleCancelAuth.bind(this);
        this.handleCancelRevokedScreen = this.handleCancelRevokedScreen.bind(this);
    }

    cardInserted(){
        this.setState({card_inserted:22893419088});
    }

    handleCancelAuth(){
        this.setState({card_inserted:false});
    }

    handleCancelRevokedScreen(){
        this.setState({card_inserted:false});
    }

    render(){
        return(
            <main className="Login">
                <h1 id="startGreeting">
                    <img src={require("../img/bank_logo.png")} alt={"Meccano banking's logo"}/>
                </h1>
                {!this.state.card_inserted &&
                    <div id="insertCard" className="authActionsContainer">
                        <h2>Insert your card</h2>
                        <button id="cardSlot" onClick={this.cardInserted}></button>
                    </div>
                }
                {this.state.card_inserted && !this.state.card_revoked &&
                    <EnterPin onCancel={this.handleCancelAuth} onSuccess={this.props.onSuccess} onRevoke={this.handleRevokedCard}/>
                }
            </main>
        )
    }
}

export default Auth;

