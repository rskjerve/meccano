import React from 'react';
import Button from '@material-ui/core/Button'
import './Auth.css';


class EnterPinColumns extends React.Component{

    render(){

        return(
               <div className="NumpadCols">
                   <Button id = {"Numpad"+this.props.rows[0]} onClick={()=> this.props.onNumber(this.props.rows[0])}>{this.props.rows[0]}</Button>
                   <Button id = {"Numpad"+this.props.rows[1]} onClick={()=> this.props.onNumber(this.props.rows[1])}>{this.props.rows[1]}</Button>
                   <Button id = {"Numpad"+this.props.rows[2]} onClick={()=> this.props.onNumber(this.props.rows[2])}>{this.props.rows[2]}</Button>
                   <Button variant="outlined" id = {this.props.rows[3]+"Btn"} onClick={()=> this.props.onOption()}
                           disabled={this.props.disableLast}>{this.props.rows[3]}</Button>
               </div>

               )
    }

}

export default EnterPinColumns;