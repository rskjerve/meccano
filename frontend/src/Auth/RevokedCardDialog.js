import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText  from '@material-ui/core/DialogContentText'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import './Auth.css'


class RevokedCardDialog extends React.Component {

    render() {
        const { onClose, ...other } = this.props;

        return (
            <Dialog className="ErrorDialogs" onClose={this.handleClose} aria-labelledby="revokedCardDialogTitle" {...other}>
                <DialogTitle id="revokedCardDialogTitle">Revoked card</DialogTitle>
                <DialogContent>
                    <DialogContent>
                        <img id="revokedCardImage" src={require("../img/error.png")} alt={"warning icon"}/>
                    </DialogContent>
                    <DialogContentText>
                          <p>Oh no! This card has been revoked</p>
                    </DialogContentText>
                    <DialogContentText>
                          <p>Contact your bank for more information</p>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.onClose} color="primary">
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}


export default (RevokedCardDialog);