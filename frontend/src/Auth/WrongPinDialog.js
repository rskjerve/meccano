import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText  from '@material-ui/core/DialogContentText'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import './Auth.css'


class WrongPinDialog extends React.Component {
    handleClose = () => {
        this.props.onClose();
    };

    render() {
        const { onClose, ...other } = this.props;
        const remaining = 3-this.props.attempts

        return (
            <Dialog className="ErrorDialogs" onClose={this.handleClose} aria-labelledby="wrong-pin-dialog-title" {...other}>
                <DialogTitle id="wrong-pin-dialog-title">Wrong pin</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        The pin you entered was incorrect
                    </DialogContentText>
                    <DialogContentText>
                        Remaining attempts: {( remaining)}
                    </DialogContentText>
                        {remaining===1 &&
                            <div id="warningText">
                                <DialogContent>
                                <img src={require("../img/error.png")} alt={"warning icon"}/>
                                </DialogContent>
                                <DialogContentText>
                                This is your last attempt before your card gets revoked.
                                    Do not re-enter unless you are sure you are right.
                                </DialogContentText>
                            </div>
                        }
                        <DialogContentText>
                            Contact your bank if you need help finding your pin code
                        </DialogContentText>
               <DialogActions>
                    <Button onClick={this.props.onCancel} color="primary" autoFocus>
                        Cancel
                    </Button>
                    <Button onClick={this.handleClose} color="primary">
                        OK
                    </Button>
                 </DialogActions>
                </DialogContent>
            </Dialog>
        );
    }
}


export default (WrongPinDialog);