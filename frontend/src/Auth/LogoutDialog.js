import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import './Auth.css'


class LogoutDialog extends React.Component {

    render() {
        const { onClose, ...other } = this.props;

        return (
            <Dialog className="ErrorDialogs" onClose={this.handleClose} aria-labelledby="LogoutDialogTitle" {...other}>
                <DialogContent id="logoutImage">
                    <img id="logoutImage" src={require("../img/logout.png")} alt="log out icon"/>
                </DialogContent>
                <DialogTitle id="logoutDialogTitle">Do you want to log out?</DialogTitle>
                <DialogContent>
                    <DialogActions id="logoutButtonBox">
                        <Button variant="outlined" onClick={this.props.onCancel} color="primary">
                            Cancel
                        </Button>
                        <Button variant="outlined" onClick={this.props.onLogout} color="primary">
                            Yes
                        </Button>
                    </DialogActions>
                </DialogContent>
            </Dialog>
        );
    }
}


export default (LogoutDialog);