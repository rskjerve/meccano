import React, { Component } from 'react';

import './Nav.css';
import NavItem from "./NavItem.js";


class NavBar extends Component{
    constructor(props) {
        super(props);
        this.state = {
            current_screen:"Home"
        };
    }


    render(){
        return(
            <nav className="NavBar">
                <a href={"#balance_box"} className="SkipLink">Skip Navigation</a>
                <ul>
                    <NavItem heading="Home" img={require('../img/home-page.png')} path="/"/>
                    <NavItem heading="Card Services" img={require('../img/card.png')} path="/card"/>
                    <NavItem heading="Gift Card" img={require('../img/gift.png')} path="/gift"/>
                    <NavItem heading="Withdraw" img={require('../img/coin-wallet.png')} path="/withdraw"/>
                    <NavItem heading="Profile" img={require('../img/profile-avatar.png')} path="/profile"/>
                    <NavItem heading="Logout" img={require('../img/logout.png')} path="/logout"/>
                </ul>
              </nav>

        );
    }
}

export default  NavBar