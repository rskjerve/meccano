import React, {Component} from 'react';
import {withRouter} from 'react-router-dom'
import './Nav.css'
import _ from "lodash";

class NavItem extends Component{
    constructor(props){
        super(props);
        const feature = _.split(this.props.history.location.pathname,'/')[1];
        let itemActive = false;
        if(feature.length===0){
          itemActive = this.props.path.length === 1;
        } else {
          itemActive = _.endsWith(this.props.path, feature);
        }
        this.state ={
            active : itemActive
        }
    }


    render(){
        return(
            <li className={this.state.active?"NavItemActive":"NavItem"}>
                <img src={this.props.img} alt=""/>
                {this.props.path &&
                <a href={this.props.path}>{this.props.heading}</a>
                }
            </li>
        );
    }
}

export default withRouter(NavItem);
