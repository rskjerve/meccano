import React from 'react';
import '../style/App.css'

class Home extends React.Component{
    render(){
        return(
            <div>
                <h1 id="wel_header" className="InfoHeader">Welcome</h1>
                <p id="wel_instructions" className="InfoText">Choose one of the options from the menubar on top of the screen</p>
            </div>
        )
    }
}

export default Home