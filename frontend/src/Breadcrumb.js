import React from 'react'
import {withRouter} from 'react-router-dom'
import './style/App.css'
import _ from 'lodash'

class Breadcrumb extends React.Component{
    constructor(props){
        super(props);

        let cr =_.compact( _.split(this.props.history.location.pathname,/[=?/]/));
        if(cr){
            const lastIndex = cr.length - 1;
            cr =  cr.map((loc, index) =>
                <li>
                    {index === lastIndex &&
                        <a href={loc} aria-current="page" disabled="">{_.capitalize(loc)}</a>

                    }
                    {index !== lastIndex &&
                        <a href={_.repeat("../",lastIndex-index)+loc} rel={_.repeat(" up",lastIndex-index)}>{_.capitalize(loc)}</a>
                    }
                </li>
            );
        }

        this.state = {
            crumbs:cr,
            last_rel:cr?_.repeat("up ",cr.length):""
        }

        this.handleRouteChange=this.handleRouteChange.bind(this);


    }

    componentDidMount(){
        this.stopListener= this.props.history.listen((location,action)=>{
            this.handleRouteChange();
        });
    }

    componentWillUnmount(){
        this.stopListener();
    }

    handleRouteChange(){
        let cr =_.compact( _.split(this.props.history.location.pathname,/[=?/]/));
        if(cr){
            const lastIndex = cr.length - 1;
            cr =  cr.map((loc, index) =>
                <li>
                    {index === lastIndex &&
                    <a href={loc} aria-current="page" disabled="">{_.capitalize(loc)}</a>

                    }
                    {index !== lastIndex &&
                    <a href={_.repeat("../",lastIndex-index)+loc} rel={_.repeat(" up",lastIndex-index)}>{_.capitalize(loc)}</a>
                    }
                </li>
            );
        }

        this.setState({'crumbs':cr});

    }


    render(){
        return(
            <nav className="Breadcrumb" aria-label="Breadcrumb">
                <a href={"#balance_box"} className="SkipLink">Skip breadcrumb</a>
                <ol>
                    <li><a href="/" rel={"index " + this.state.last_rel}>Home</a></li>
                {this.state.crumbs &&
                this.state.crumbs
                }
                </ol>
            </nav>

        )
    }
}

export default withRouter(Breadcrumb);