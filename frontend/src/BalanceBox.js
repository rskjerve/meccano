import React from 'react';
import './style/App.css'
import {withCustomerContext} from './Customer'

class BalanceBox extends React.Component{
    render(){
        return(
            <aside id="balance_box" className="BalanceBox">
                <a href={"#main_content"} className="SkipLink">Skip to main content</a>
                <h2>{this.props.customer || "Balance Information"}</h2>
                <table aria-label="Semantic Table Elements">
                    <tbody>
                    <tr>
                     <th>Balance:</th>
                     <td>{this.props.balance + " " + this.props.currency|| "information not available"}</td>
                    </tr>
                    </tbody>
                </table>
            </aside>
        )
    }
}

export default withCustomerContext(BalanceBox);