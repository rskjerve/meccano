import axios from 'axios';
import jwt from 'jsonwebtoken';

class Request{

    constructor(){
        this.baseUrl = 'http://localhost:3001/';
        axios.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('token');

    }

    /**
     * Request auth
     * API: /auth,{card,pin}
     * @param card
     * @param pin
     */
    apiAuth(insertedCard,enteredPin,callback){
        const req = {
            method:'post',
            url:this.baseUrl+'auth',
            data:{
                card:insertedCard,
                pin:enteredPin
            }
        };
        axios(req)
            .then(data=>{
                  callback.success(data.data)
                })
                .catch(error =>{
                    if(error.response && error.response.status === 401){
                        callback.error(error.response.data);
                    }
                });
    }

    apiGet(token,endpoint){
     let req = {
         method:'get',
         url:this.baseUrl+endpoint
     };
     this.securityPrep(req,endpoint);
    }

    /**
     *
     * @param token
     * @param body
     * @param endpoint
     * @param callback: optional object with callback functions for success and failure
     */
    apiPost(body, endpoint,callback){
        let req = {
            method:'post',
            url:this.baseUrl+endpoint,
            data: body
        };


        Request.securityPrep(req,endpoint);
        Request.bodyPrep(req, body);

        // Send a POST request

        axios(req)
            .then(data =>{this.handleSuccess(data,callback.success);})
            .catch(error => {
                if (error.response){
                    if(error.response.status === 401){
                        this.handleUnauthorized();
                    } else {
                        this.handleError(error.response, callback.error)
                    }
                } else {console.log('uncaught error',error)}
            });
    }

    static securityPrep(httpReq, token){
        //TODO
        //encrypt
    }

    static bodyPrep(httpReq, body){
        httpReq.headers={'Content-Type': 'application/json'}
        httpReq.data=body;
    }

    handleSuccess(data, callback){
        if(callback){
            callback(data);
        }
    }

    handleError(error, callback){
        if(callback){
            callback(error)
        }
    }

    handleUnauthorized(){
        localStorage.removeItem('token');
    }

    static getCustomerFromToken(token){
        const data = this.decode(token);
        return {name:data.payload.name, balance:data.payload.balance};
    }

    static decode(token){
        return jwt.decode(token, {complete: true});
    }

}

export default Request;